# Changelog for pemfung-finance

## Unreleased changes
### 19 November 2019
- Change `id` for European Option from `c11` to `european`
- European Option is now defined as an European Option Contract that will give either a Zero-Coupon Bond at horizon or Zero beyond that

### 26 November 2019
- ExContr is now defined as 
```haskell
data ContrPart = ContrPart { horizon :: Double
                           , value :: Double
                           , parts :: [(String, Double, Double)]
                           } deriving (Read, Show, Eq, Generic, ToJSON, FromJSON)
data ExContr = ExContr { contract :: String
                       , contractPart :: ContrPart
                       , lattice :: Bool
                       } deriving (Read, Show, Eq, Generic, ToJSON, FromJSON)
```
This means that the json structure will roughly be like this:
```json
{
	"contract": "european",
	"contractPart": {
		"horizon": 5.0,
		"value": 0,
		"parts": [
			["get", 15, 10], ["get", 20, 15]
		]
	},
	"lattice": true
}
```
This contract gives the right to choose, at 5 unit of time from now, whether or not to acquire an underlying contrat consisting of two ZCB Contract which will give 10 USD at 15 unit of time from now and give 15 USD at 20 unit of time from now.

Another example:
```json
{
	"contract": "zcb",
	"contractPart": {
		"horizon": 5.0,
		"value": 10.0,
		"parts": []
	},
	"lattice": true
}
```
This contract is a ZCB that will give 10 USD at 5 unit of time from now.

This data structure is not perfect yet. We will further improve it to allow it greater flexibility.

# 3 December 2019
Some example of JSON Structure

```json
{
	"exEval": {
		"tag": "EvalContract",
		"contents": {
			"tag": "ZCB",
			"time": 1,
			"value": 20.0,
			"currency": "USD"
		}
	},
	"imageType": "png"
}
```

```json
{
	"exEval": {
		"tag": "EvalContract",
		"contents": {
			"tag": "European",
			"time": 5,
			"contract": {
				"tag": "AndContracts",
				"contracts": [
					{
						"tag":"ZCB",
			            "time": 3,
			            "value": 5,
			            "currency":"USD"
					},
					{
						"tag":"GiveContract",
			            "contract": {
		            		"tag":"ZCB",
			            	"time": 5,
				            "value": 10,
				            "currency":"USD"
		            	}	
					}
				]
			}
		}
	},
	"imageType": "png"
}
```
Above JSON describe this contract

```european (mkDate 5) ((zcb (mkDate 3) 5 USD) `andGive` (zcb (mkDate 5) 10 USD))```