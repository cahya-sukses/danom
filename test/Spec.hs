{-# LANGUAGE LambdaCase #-}

import           Contracts
import           Control.Exception (evaluate)
import           Errors
import           Test.Hspec
import           Test.QuickCheck

main :: IO ()
main = hspec $ do
  describe "evalEx" $ do
    it "returns ContractNotFound when given invalid contract type" $ do
      evalEx (ExContr ("casda", [], "png")) `shouldSatisfy` \case
        Left (ContractNotFound _) -> True
        _                         -> False
