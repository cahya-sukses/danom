# pemfung-finance

## Deployment
- Frontend Service deployed on https://omonib.netlify.com
- Backend Service deployed on https://danom.cahyanugraha12.me
- Slides can be accessed on https://docs.google.com/presentation/d/14kCAk7DozntqAYUXQ5u8KhTTM9CYWAfwByuxsnF9d_s/edit?usp=sharing

## Installation
- Install **stack** and **graphviz** on your computer
  - For **stack**, you can go to http://haskell.org/platform
  - For **graphviz**, on Ubuntu you can install it with command `sudo  apt-get install graphviz`
- Clone this repository
- Run command `stack build`
- Run command `stack run`
- The application should be running on port 3001

## Danom Endpoints

> GET /status

Check if the backend services is up or not.
Will simply return a `json` like below if the service is up.

```json
{
  "message": "on"
}
```

> POST /contract-ex

Get an evaluation lattice depending on the contract type and argument.
Return a `json` that give the url for the lattice image.

Example Request:

This request describe a ZCB contract that has 20 USD Value and horizon 1 time unit from now.
```json
{
	"exEval": {
		"tag": "EvalContract",
		"contents": {
			"tag": "ZCB",
			"time": 1,
			"value": 20.0,
			"currency": "USD"
		}
	},
	"imageType": "png"
}
```

200 Success Response:

```json
{
  "imgUrl": "/imgtmp/pr-lattice3.svg"
}
```

422 Failed Response (Not Enough Arguments):

```json
{
  "message": "Not enough argument given, expected 2"
}
```

422 Failed Response (Invalid Contract Type):

```json
{
  "message": "c22 not found in examples"
}
```

## Contract Types

Example Contract:
European Contract with underlying ZCB.

```json
{
	"exEval": {
		"tag": "EvalContract",
		"contents": {
			"tag": "European",
			"time": 5,
			"contract": {
				"tag": "AndContracts",
				"contracts": [
					{
						"tag":"ZCB",
			            "time": 3,
			            "value": 5,
			            "currency":"USD"
					},
					{
						"tag":"GiveContract",
			            "contract": {
		            		"tag":"ZCB",
			            	"time": 5,
				            "value": 10,
				            "currency":"USD"
		            	}	
					}
				]
			}
		}
	},
	"imageType": "png"
}
```

Each contract is defined by its tag. Currently there are 6 contract tags:

```
ZCB
European
American
GiveContract
AndContracts
OrContracts
```

Each tag requires their own properties described below:

- Zero-Coupon Bond (ZCB)

  tag: `ZCB`

  currency: USD | GBP | EUR | ZAR | KYD | CHF

  time: contract horizon (Integer)

  value: value of the contract in `currency`

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "ZCB",
        "time": 1,
        "value": 20.0,
        "currency": "USD"
      }
    },
    "imageType": "png"
  }
  ```

- European Option

  tag: `European`

  time: contract horizon (Integer)

  contract: the underlying contact given by the European Option

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "European",
        "time": 5,
        "contract": {
            "tag":"ZCB",
            "time": 3,
            "value": 5,
            "currency":"USD"
          }
        }
      }
    },
    "imageType": "png"
  }
  ```

- American Option

  tag: `American`

  timeAfter: unit of time denoting that the contract can be exercised after that unit of time (Integer)

  timeBefore: unit of time denoting that the contract can be exercised before that unit of time (Integer)

  Not that timeAfter must be lower that timeBefore

  contract: the underlying contact given by the American Option

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "American",
        "timeAfter": 2,
        "timeBefore": 5,
        "contract": {
            "tag":"ZCB",
            "time": 3,
            "value": 5,
            "currency":"USD"
          }
        }
      }
    },
    "imageType": "png"
  }
  ```

- Give Contract

  tag: `GiveContract`

  Denote a contract that is inversed, i.e. a contract that give us the obligation to pay a certain amount of currency at a time

  contract: the contract that we want to be inversed

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "European",
        "time": 5,
        "contract": {
            "tag":"GiveContract",
            "contract": {
              "tag":"ZCB",
              "time": 5,
              "value": 10,
              "currency":"USD"
            }
        }
      }
    },
    "imageType": "png"
  }
  ```

- AndContracts

  tag: `AndContracts`

  Denote a series of contract that are bundled together.

  contracts: a list of contract that we want to be bundle together

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "European",
        "time": 5,
        "contract": {
            "tag":"AndContracts",
            "contracts": [
              {
                "tag":"ZCB",
                "time": 5,
                "value": 10,
                "currency":"USD"
              },
              {
                "tag":"ZCB",
                "time": 5,
                "value": 20,
                "currency":"USD"
              }
            ]
        }
      }
    },
    "imageType": "png"
  }
  ```

- Or Contracts

  tag: `OrContracts`

  Denote a series of contract that are mutually exclusive.

  contracts: a list of contract that we want to be set as mutually exclusive with each other.

  Example

  ```json
  {
    "exEval": {
      "tag": "EvalContract",
      "contents": {
        "tag": "European",
        "time": 5,
        "contract": {
            "tag":"OrContracts",
            "contracts": [
              {
                "tag":"ZCB",
                "time": 5,
                "value": 10,
                "currency":"USD"
              },
              {
                "tag":"ZCB",
                "time": 5,
                "value": 20,
                "currency":"USD"
              }
            ]
        }
      }
    },
    "imageType": "png"
  }
  ```

## Supported imageTypes

`png` `svg`
