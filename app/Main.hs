module Main where

import           Data.Maybe
import           DemoContracts
import           System.Environment

main :: IO ()
main = do
  port <- fromMaybe "3001" <$> lookupEnv "SERVER_PORT"
  putStrLn $ "Running server on port " ++ port
  server $ read port
