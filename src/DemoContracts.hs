{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module DemoContracts where

import           Contracts                     hiding (currency, time, value)
import           Control.Monad
import           Control.Monad.Reader
import           Control.Monad.Trans           (liftIO)
import           Data.Aeson                    hiding (json)
import qualified Data.Text.Lazy                as LT
import           Errors
import           GHC.Generics
import           Network.HTTP.Types.Status
import           Network.Wai                   (Middleware, mapResponseHeaders,
                                                modifyResponse)
import           Network.Wai.Middleware.Cors
import           Network.Wai.Middleware.Static
import           Numeric
import           Web.Scotty.Trans

instance ToJSON ExContr
instance FromJSON ExContr
instance ToJSON LatticeImage
instance FromJSON LatticeImage
instance ToJSON ExEval
instance FromJSON ExEval
instance FromJSON Currency
instance ToJSON Currency

data Response = Response { message :: String } deriving (Show, Generic)
instance ToJSON Response
instance FromJSON Response

data ContractExResponse = ContractExResponse { imgUrl :: String } deriving (Show, Generic)
instance ToJSON ContractExResponse

data ContractExampleOption = ContractExampleOption { tag :: String, name :: String, time :: Int, value :: Double, currency :: String } deriving (Show, Generic)
instance ToJSON ContractExampleOption

exampleOptions = [
    ContractExampleOption { tag = "zcb", name = "Zero-coupon bond", time = 3, value = 20.0, currency = "USD" }
  ]

data Env = Env { imgPath :: String }

addXSSProtection::Middleware
addXSSProtection =
  modifyResponse (mapResponseHeaders (("X-XSS-Protection", "1; mode=block") :))

addAntiMIMESniffing::Middleware
addAntiMIMESniffing =
    modifyResponse (mapResponseHeaders (("X-Content-Type-Options", "nosniff") :))

server :: Int -> IO ()
server port = do
  let env = Env { imgPath = "imgtmp/" }
  scottyT port (flip runReaderT env) routes

routes :: ScottyT LT.Text (ReaderT Env IO) ()
routes = do
  middleware $ simpleCors
  options (regex ".*") $ return ()

  middleware $ addXSSProtection
  middleware $ addAntiMIMESniffing
  middleware $ staticPolicy (noDots >-> isNotAbsolute >-> hasPrefix "imgtmp")

  post "/contract-ex" $ do
    contract <- jsonData
    maybeImgUrl <- liftIO $ renderExImg contract
    case maybeImgUrl of
      Right url -> json $ ContractExResponse { imgUrl = url }
      Left err -> do
        case err of
          LatticeImageError exitCode -> status internalServerError500
          _                          -> status unprocessableEntity422
        json Response { message = (show err) }

  get "/status" $ do
    json Response { message = "on" }

  get "/examples" $ do
    json $ exampleOptions
