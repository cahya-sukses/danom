module Errors where

import           System.Exit

data GenerateContractImageError = LatticeImageError ExitCode | ArgsNotEnough Int | ContractNotFound String

instance Show GenerateContractImageError where
    show (LatticeImageError exitCode) = "System error when generating lattice image with status code: " ++ (show exitCode)
    show (ArgsNotEnough argCount) = "Not enough argument given, expected " ++ (show argCount)  
    show (ContractNotFound contractId) = contractId ++ " not found in examples"
