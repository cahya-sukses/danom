FROM haskell:8.6.5

COPY . /opt

WORKDIR /opt

RUN apt-get update
RUN apt-get install graphviz -y

RUN stack build

CMD ["stack","exec","pemfung-finance-exe"]